from __future__ import print_function
from __future__ import division
from . import C_

import pandas as pd
import numpy as np

def get_split_model_key_value_df(df, all_model_keys, model_col_name:str):
	'''
	Return a new DataFrame with the splited model attributes encoded in model name.
	Given a DF and model_col_name, return the model keys-values.
	The model name need to have the following format: key1-value1_key2-value2_...
	'''
	rows_list = []
	for index,row in df.iterrows():
		row_dict = {mk:np.nan for mk in all_model_keys}
		for model_keys_values in row[model_col_name].split(C_.KEY_KEY_SEP_CHAR):
			key_value = model_keys_values.split(C_.KEY_VALUE_SEP_CHAR)
			assert len(key_value)==2
			key, value = key_value
			row_dict[key] = value

		rows_list.append(row_dict)

	df = pd.DataFrame.from_dict(rows_list, orient='columns')
	return df

def get_bar_latex(model_attributes:int, results:int):
	'''
	Return a latex table align format string.
	Ej: lcc|ccccc
	The bar is used to separate the model attributes from the results
	'''
	bar = 'l'+'c'*(model_attributes-1)+'|'+'c'*(results)
	return bar

def brackets(s:str):
	return '{'+s+'}'

def get_max_obj_bound(objs, bound='max', f=1):
	values = np.array([getattr(o, bound)(f) for o in objs])
	index = np.argmax(values)
	return objs[index], values[index], index

def get_min_obj(objs):
	return get_max_obj(objs, f=-1)

def get_max_obj(objs, f=1):
	assert len(objs)>0
	obj1, v1, index = get_max_obj_bound(objs, 'min', f)
	if len(objs)==1:
		return obj1, 0, False
	_, v2, _ = get_max_obj_bound([o for o in objs if not o==obj1], 'max', f)
	signif_bigger = v1>v2
	return obj1, index, signif_bigger
	
class MeanStd(object):
	def __init__(self, mean, std,
		float_round_mean:int=C_.MEAN_DECIMALS,
		float_round_std:int=C_.STD_DECIMALS,
		print_std:bool=True,
		uses_std_min_max:bool=True,
		):
		if np.isnan(mean) or np.isnan(std):
			self.is_nan = True
			std = 0
		else:
			self.reset()

		assert std>=0
		self.mean = mean
		self.std = std
		self.float_round_mean = float_round_mean
		self.float_round_std = float_round_std
		self.print_std = print_std
		self.uses_std_min_max = uses_std_min_max
		
	def __repr__(self):
		if self.is_nan:
			return f'{C_.NAN_LATEXCHAR}'
		mean_text = f'{int(self.mean):,}' if isinstance(self.mean, int) or self.mean.is_integer() else f'{round(self.mean, self.float_round_mean):,}'
		std_text = '' if not self.print_std or self.std==0 else f'{C_.SUM_MINUS_LATEXCHAR}{round(self.std, self.float_round_std):,}'
		return self.style[0]+f'{mean_text}{std_text}'+self.style[1]

	def reset(self):
		self.is_nan = False
		self.is_bold = False
		self.style = '{','}'
		
	def set_bold(self):
		self.is_bold = True
		self.style = '\\textbf{','}'
		
	def min(self, f=1):
		if self.is_nan:
			return -np.infty
		return self.mean*f - (self.std if self.uses_std_min_max else 0)
	
	def max(self, f=1):
		if self.is_nan:
			return -np.infty
		return self.mean*f + (self.std if self.uses_std_min_max else 0)
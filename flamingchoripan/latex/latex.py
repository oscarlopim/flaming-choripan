from __future__ import print_function
from __future__ import division
from . import C_

import copy
import pandas as pd
import numpy as np
from ..myUtils import strings as strings
from ..myUtils import prints as prints
from . import utils as utils

###################################################################################################################################################

class LatexTable(object):
	'''
	Class used to convert a dictionary of model results and experiments in a latex string to just copy-paste in overleaf.
	Also, this return a DataFrame object.
	This class can bold the best result along a column (experiment) given a criterium: maximum or minimum.
	Also, this add the standar deviation of several results from every model results. Ej: 10 +-0.1
	You can use subtables, each one with local independent criteriums separated by an horizontal line.

	The format of the table is:
	------------------------------------------------------------------------------------------------------------
	model_att_1, model_value_1, model_att_2, model_value_2, ... | experiment_1, experiment_2, experiment_3, ...
	------------------------------------------------------------------------------------------------------------
	A1           a1             X1           x1                 | 10 +-0.1      ...
	B1           b1             Y1           y1                 | 18 +-0.01     ...
	C1           c1             Z1           z1                 | 89 +-0.75     ...
	------------------------------------------------------------------------------------------------------------
	A2           a2             X2           x2                 | 80 +-1.1      ...
	B2           b2             Y2           y2                 | 68 +-0.71     ...
	C2           c2             Z2           z2                 | 87 +-2.75     ...
	------------------------------------------------------------------------------------------------------------
	'''
	def __init__(self, model_results_dic:dict,
		bold_criteriums:list=None,
		print_std:bool=True,
		uses_std_min_max:bool=True,
		replace_dic:dict={},
		delete_redundant_model_keys:bool=True,
		colored_criterium_row:bool=True,
		float_round_mean:int=C_.MEAN_DECIMALS,
		float_round_std:int=C_.STD_DECIMALS,
		):
		'''
		Parameters
		----------
		model_results_dic: It is a dictionary where every key correspond to the model name (coded as: attribute1-value1_attribute2-value2_...).
											Every sub-key correspond to the experiment results.
											Every element in the dict must be a tuple of the form (mean, std).
											Important: If you pass the argument as list[dic] you can build several independent criterium subtables separated by an horizontal line.
		bold_criteriums: A list with dimensions <results> where every value correspond to the criterium used for every experiment.
											The criterium takes care of the std range!!!
											max: maximum criterium to bold.
											min: use minimum criterium to bold.
											None: don't bold
											Important: If you have a multi-subtables format, you need to use list[array[int]]
		dropped_model_attributes: list of model attributes that will no be present in the final latex table.
											It's often used when every model has the same key-value values, so it's innecesary to show that information in the table.
		print_std: If True, print the std in the latex table results.
		replace_dic: dictionary used to replace model strings. see strings.string_replacement for more information
		'''
		self.model_results_dic = copy.deepcopy(model_results_dic)
		if not isinstance(model_results_dic, list):
			self.model_results_dic = [self.model_results_dic]

		self.results_columns = self.get_results_columns()
		self.bold_criteriums = self.get_bold_criteriums(bold_criteriums)
		self.print_std = print_std
		self.uses_std_min_max = uses_std_min_max
		self.replace_dic = replace_dic
		self.delete_redundant_model_keys = delete_redundant_model_keys
		self.colored_criterium_row = colored_criterium_row
		self.float_round_mean = float_round_mean
		self.float_round_std = float_round_std
		self.dicts_to_dfs()
		self.split_model_key_value_dfs()

	def get_results_columns(self):
		results_columns = []
		for key in self.model_results_dic[0].keys():
			cols = list(self.model_results_dic[0][key].keys())
			#print(cols)
			results_columns.append(cols)

		for rc in results_columns:
			assert len(results_columns[0])==len(rc)

		return results_columns[0]

	def get_bold_criteriums(self, bold_criteriums):
		if bold_criteriums is None:
			bold_criteriums = [[None for rc in self.results_columns] for mrd in self.model_results_dic]

		if isinstance(bold_criteriums, str):
			bold_criteriums = [[bold_criteriums for rc in self.results_columns] for mrd in self.model_results_dic]

		#print('bold_criteriums',bold_criteriums)
		assert len(bold_criteriums)==len(self.model_results_dic)
		for w,_ in enumerate(self.model_results_dic):
			for k,key in enumerate(self.model_results_dic[w].keys()):
				assert len(bold_criteriums[w])==len(self.model_results_dic[w][key].keys())

		return bold_criteriums

	def dicts_to_dfs(self):
		self.dfs_row_colors = []
		self.dfs = []
		for kmr,dic in enumerate(self.model_results_dic):
			df = pd.DataFrame.from_dict(dic, orient='index').reset_index()
			df.columns = [C_.DEFAULT_MODEL_COL_NAME]+self.results_columns
			row_colors = []
			for index,row in df.iterrows():
				for col in self.results_columns:
					values = row[col]
					assert len(values)==2
					row[col] = utils.MeanStd(*values, self.float_round_mean, self.float_round_std, self.print_std, self.uses_std_min_max)

				row[C_.DEFAULT_MODEL_COL_NAME] = strings.string_replacement(row[C_.DEFAULT_MODEL_COL_NAME], self.replace_dic)
				row_colors.append(None)

			for kc,col in enumerate(self.results_columns):
				signif_bigger = False
				bold_criterium = self.bold_criteriums[kmr][kc]
				if not bold_criterium is None:
					objs = [row[col] for index,row in df.iterrows()]
					if bold_criterium=='min':
						obj, index, signif_bigger = utils.get_min_obj(objs)
					elif bold_criterium=='max':
						obj, index, signif_bigger = utils.get_max_obj(objs)
					else:
						raise Exception(f'invalid bold_criterium: {bold_criterium}')
				
				if signif_bigger:
					obj.set_bold()
					if self.colored_criterium_row:
						row_colors[index] = C_.C_LIGHT_GRAY

			#print(df, row_colors)
			self.dfs_row_colors.append(row_colors)
			self.dfs.append(df)

	def split_model_key_value_dfs(self):
		all_model_keys_dict = {}
		models_count = 0
		for k,df in enumerate(self.dfs):
			model_keys_values = []
			for x in list(df[C_.DEFAULT_MODEL_COL_NAME]):
				models_count += 1
				model_keys_values += x.split(C_.KEY_KEY_SEP_CHAR)

			for model_key_value in model_keys_values:
				key_value = model_key_value.split(C_.KEY_VALUE_SEP_CHAR)
				assert len(key_value)==2
				key, value = key_value
				if not key in all_model_keys_dict.keys():
					all_model_keys_dict[key] = []
				all_model_keys_dict[key].append(value)

		model_keys = list(all_model_keys_dict.keys())
		redundant_model_keys = [mk for mk in model_keys if np.all(np.array(all_model_keys_dict[mk])==all_model_keys_dict[mk][0]) and len(all_model_keys_dict[mk])==models_count]
		unique_model_keys = [mk for mk in model_keys if not mk in redundant_model_keys]
		#print(model_keys, unique_model_keys, redundant_model_keys)
		#assert 0
		self.model_keys = unique_model_keys if self.delete_redundant_model_keys else model_keys
		for k,df in enumerate(self.dfs):
			split_model_key_df = utils.get_split_model_key_value_df(df, self.model_keys, C_.DEFAULT_MODEL_COL_NAME)
			df = pd.concat([split_model_key_df, df[self.results_columns]], axis=1)
			df = df.fillna(C_.NAN_LATEXCHAR)
			self.dfs[k] = df
			#print(df)

	def print_top(self):
		txt = ''
		for k,c in enumerate(self.model_keys+self.results_columns):
			txt += '{\\color[HTML]{FFFFFF}'+c.replace('_', C_.NEW_LATEXCHAR)+'}'
			txt += '}&' if k==0 else '&'
		txt = txt[:-1]
		txt += '\\\\[0.25ex]'
		txt += '\n'
		print(txt)

	def print(self,
		caption:str='???',
		label:str='???',
		centered:bool=True,
		custom_tabular_align:str=None, # 'ccc|llll'
		):
		'''
		Parameters
		----------
		caption: the caption used in latex
		label: the label used in latex
		centered: If True, use the \begin{table*} ... \end{table*} format.
				If False, use the \begin{table}[H] ... \end{table} format.
		'''
		prints.print_bar(char='%')
		self.print_init(caption, label, len(self.model_keys), len(self.results_columns), centered, custom_tabular_align)
		self.print_top()
		for row_colors,df in zip(self.dfs_row_colors, self.dfs):
			for index,row in df.iterrows():
				#print('row',row)
				row_color = '' if row_colors[index] is None else f'\\rowcolor[HTML]{utils.brackets(row_colors[index][1:])} '
				print(row_color+' & '.join([row[c] for c in self.model_keys]+[row[c].__repr__() for c in self.results_columns])+' \\\\')

			print(f'{C_.LATEX_HLINE}\n')

		self.print_end(centered)
		prints.print_bar(char='%')

	def print_init(self,
		caption:str,
		label:str,
		model_keys:int,
		results_columns:int,
		centered:bool,
		custom_tabular_align:str,
		):
		txt = ''
		txt += '\\begin{table*}'+'\n' if centered else '\\begin{table}[H]'+'\n'
		txt += '\\centering'+'\n'
		txt += '\\caption{'+caption+'}'+'\n'
		txt += '\\label{'+label+'}\\vspace{.1cm}'+'\n'
		tabular_align = utils.get_bar_latex(model_keys, results_columns) if custom_tabular_align is None else custom_tabular_align
		txt += '\\begin{tabular}{'+tabular_align+'}\\rowcolor[HTML]{'+C_.C_HARD_GRAY[1:]+'}\n'
		print(txt)

	def print_end(self, centered):
		txt = ''
		txt += '\\end{tabular}'+'\n'
		txt += '\\end{table*}'+'\n' if centered else '\\end{table}'+'\n'
		print(txt, end='')


#############################################################

def sanity_check():
	'''
	Used to check if all is in order!!!!
	'''
	accu_res_dic = {
	'mdl-A_a-4_b-1_c-1_fff-9':{'a*':(1,0.0),'b*':(2,0.0),'c*':(3,0.3)},
	'mdl-B_a-4_c-2_b-10':{'a*':(2,0.1),'b*':(5,0.0),'c*':(66,0.0003)},
	'mdl-C_a-4_c-3':{'a*':(19,.11),'b*':(10,.21),'c*':(8,.31)},
	'mdl-D_a-4_c-4_gg-99':{'c*':(21,.11),'b*':(10,.11),'a*':(8,.31)},
	}
	alone_model = {
	'mdl-F_a-4_b-1_c-1_ufff-men':{'a*':(1,.0),'b*':(2,.0),'c*':(3,.3)},
	'mdl-F_a-4_b-1_c-9_ufff-men':{'a*':(1,.0),'b*':(2,.0),'c*':(9,.3)},
	}
	model_results_dic = accu_res_dic
	model_results_dic = [accu_res_dic, alone_model]

	kwargs = {
		'delete_redundant_model_keys':False,
		'colored_criterium_row':True,
	}
	
	for bold_criteriums in [None, 'min', 'max']:
		latex_table = LatexTable(model_results_dic, bold_criteriums=bold_criteriums, **kwargs)
		label = 'results_columns'
		latex_table.print(label=label, centered=0)
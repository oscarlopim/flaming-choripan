from __future__ import print_function
from __future__ import division
from . import C_

import os
import time

class Cronometer(object):
	def __init__(self):
		self.reset()

	def reset(self):
		'''
		Refresh the initial time of the Cronometer
		The time is measured in seconds
		'''
		self.it_pt = time.perf_counter() # in segs

	def dt_mins(self,
		reset:bool=False,
		):
		return self.dt_segs(reset)/60.

	def dt_segs(self,
		reset:bool=False,
		):
		dt_pt = (time.perf_counter()-self.it_pt)
		if reset:
			self.reset()
		return dt_pt

	def print(self,
		reset:bool=False,
		):
		dt_pt = self.dt_segs(reset)
		print(f'time.perf_counter: {dt_pt:.3f}[segs] {dt_pt/60.:.3f}[mins] {dt_pt/60./60.:.3f}[hrs]')
		return dt_pt
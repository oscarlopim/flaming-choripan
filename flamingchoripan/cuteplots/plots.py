from __future__ import print_function
from __future__ import division
from . import C_

from ..datascience.statistics import get_populations_cdict
from . import colors as cc
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import math

###################################################################################################################################################

def plot_bar(data_dict:dict, color_legends:list,
	annotations_dict:dict=None,
	bar_width:float=0.5,
	rotate_xlabel:bool=False,
	uses_log_scale:bool=False,
	uses_legend:bool=True,
	uses_bottom_legend:bool=True, # change for mode
	legend_ncol:int=None,
	add_percent_annotations:bool=True,

	fig=None,
	ax=None,
	figsize:tuple=C_.PLOT_FIGSIZE_BOX,
	#xlabel:str='values',
	ylabel:str=None,
	title:str='plot_bar',
	#xlim:tuple=[None, None],
	#ylim:tuple=[None, None],
	grid:bool=True,
	grid_alpha:float=C_.PLOT_GRID_ALPHA,
	legend_loc:str='upper center',
	cmap=None,
	alpha:float=1,
	verbose:int=0,
	**kwargs):
	ylabel = 'population' if ylabel is None else ylabel

	fig, ax = (plt.subplots(1,1, figsize=figsize, dpi=C_.PLOT_DPI) if fig is None else (fig, ax))
	if not isinstance(data_dict, dict):
		data_dict = {'bar plot':data_dict} # transform in a dummy dict
	keys = list(data_dict.keys())
	cmap = cpc.get_default_cmap(len(keys)) if cmap is None else cmap

	### anotations
	if add_percent_annotations:
		keys = data_dict.keys()
		sub_keys = data_dict[list(data_dict.keys())[0]].keys()
		annotations_dict = {k:{sk:f'{100*data_dict[k][sk]/sum([data_dict[k2][sk] for k2 in keys]):.2f}%' for sk in sub_keys} for k in keys}

	x = np.arange(len(keys))
	legend_handles = []
	maximum_value = 0
	for kl,legend_name in enumerate(color_legends):
		list_values = []
		c = cmap.colors[kl]
		legend_handles.append(mpatches.Patch(color=c, label=legend_name))
		new_x = x - bar_width/2. + kl/float(len(color_legends))*bar_width
		new_bar_width = bar_width/float(len(color_legends))
		for k,set_name in enumerate(keys):
			assert legend_name in data_dict[set_name].keys()
			value = data_dict[set_name][legend_name]
			list_values.append(value)
			maximum_value = max(maximum_value, value)

			if not annotations_dict is None:
				size = 10
				ann_x = new_x[k] + bar_width/float(len(color_legends))/2
				ann_y = value
				ann_text = annotations_dict[set_name][legend_name]
				ann = ax.annotate(ann_text, xy=(ann_x, ann_y), xytext=(0, size*1.5), fontsize=5, ha='center',
						textcoords='offset points',
						size=size, va='center', color='w',
						bbox=dict(boxstyle="round", fc=c, ec="none", alpha=alpha),
						arrowprops=dict(arrowstyle='wedge,tail_width=1.0', fc=c, ec='none', patchA=None, patchB=None, relpos=(0.5, 0.5)),
						)
		
		ax.bar(new_x, list_values, width=new_bar_width, align='edge', color=c, alpha=alpha)

	ax.set_xticks(x)
	rotation = 45 if rotate_xlabel else 0
	ha = 'right' if rotate_xlabel else 'center'
	ax.set_xticklabels(keys, rotation=rotation, ha=ha, rotation_mode='anchor')

	#### LEGEND
	if len(color_legends)>1 and uses_legend:
		ax.legend(handles=legend_handles)
		if uses_bottom_legend:
			legend_ncol = len(color_legends) if legend_ncol is None else legend_ncol
			ax.legend(handles=legend_handles, loc=legend_loc, bbox_to_anchor=(0.5, -0.15), shadow=True, ncol=legend_ncol)	

	ax.set_yscale('log' if uses_log_scale else 'linear')
	offset_factor = (math.exp(0.2) if uses_log_scale else 0.1)
	ax.set_ylim([ax.get_ylim()[0], ax.get_ylim()[1] + ax.get_ylim()[1]*offset_factor])

	#### OTHER FORMATS
	if grid:
		#xax = ax.set_axisbelow(True); ax.xaxis.grid(True, alpha=grid_alpha)
		yax = ax.set_axisbelow(True); ax.yaxis.grid(True, alpha=grid_alpha)

	ax.set_ylabel(ylabel)
	ax.set_title(title)
	return fig, ax

def plot_hist_labels(values_dict_raw:dict, classes_names:list,
	bar_width:float=0.5,
	add_percent_annotations:bool=True,
	rotate_xlabel:bool=False,
	uses_log_scale:bool=False,
	count_scale=1,
	**kwargs):
	'''
	values_dict_raw = set_names: values
	'''
	values_dict = values_dict_raw.copy()
	if not isinstance(values_dict, dict):
		values_dict = {'dataset':values_dict}

	legends = list(values_dict.keys())
	data_dict = {}
	for k,key in enumerate(values_dict.keys()): # the sets
		values = values_dict[key]
		populations_cdict = get_populations_cdict(values, classes_names)

		for c in populations_cdict.keys():
			if not c in data_dict.keys():
				data_dict[c] = {key:{}}
			data_dict[c][key] = populations_cdict[c]

	return plot_bar(data_dict, legends,
		bar_width=bar_width,
		rotate_xlabel=rotate_xlabel,
		uses_log_scale=uses_log_scale,
		add_percent_annotations=add_percent_annotations,
		**kwargs)

def plot_hist_discrete(data_dict:dict,
	bar_width:float=0.5,
	uses_bottom_legend:bool=True,
	legend_ncol:int=None,
	uses_density:bool=False,

	fig=None,
	ax=None,
	figsize:tuple=C_.PLOT_FIGSIZE,
	xlabel:str='values',
	ylabel:str='population',
	title:str='plot_hist_discrete',
	xlim:tuple=[None, None],
	ylim:tuple=[None, None],
	grid:bool=True,
	grid_alpha:float=C_.PLOT_GRID_ALPHA,
	legend_loc:str='upper center',
	cmap=None,
	alpha:float=1,
	verbose:int=0,
	**kwargs):

	fig, ax = (plt.subplots(1,1, figsize=figsize, dpi=C_.PLOT_DPI) if fig is None else (fig, ax))
	if not isinstance(data_dict, dict):
		data_dict = {'distribution':data_dict} # transform in a dummy dict
	keys = list(data_dict.keys())
	cmap = (cpc.get_default_cmap(len(keys)) if cmap is None else cmap)

	for k,key in enumerate(keys):
		values = data_dict[key]
		uniques, counts = np.unique(values, return_counts=True)
		if uses_density:
			counts_sum = counts.sum()
			counts = [c/counts_sum for c in counts]

		c = cmap.colors[k]
		x = uniques
		new_bar_width = bar_width/float(len(keys))
		new_x = x - bar_width/2. + k/float(len(keys))*bar_width+new_bar_width*0.5
		ax.bar(new_x, counts, width=new_bar_width, color=c, label=key)

	#### LEGEND
	if len(keys) > 1:
		if uses_bottom_legend:
			legend_ncol = (len(keys) if legend_ncol is None else legend_ncol)
			ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), shadow=True, ncol=legend_ncol)
		else:
			ax.legend()

	#### LIMITS
	ax.set_xlim(xlim)
	ax.set_ylim(ylim)
	
	#### OTHER FORMATS
	if grid:
		#xax = ax.set_axisbelow(True); ax.xaxis.grid(True, alpha=grid_alpha)
		yax = ax.set_axisbelow(True); ax.yaxis.grid(True, alpha=grid_alpha)

	ax.set_xlabel(xlabel)
	ax.set_ylabel(ylabel)
	ax.set_title(title)
	return fig, ax

def plot_hist_bins(data_dict:dict,
	bins:int=500,
	linewidth:float=2,
	uses_density:bool=False,
	uses_annotation:bool=True,
	annotation_size:int=9,
	annotation_alpha:float=1,
	histtype:str='step', # step, bar, stepfilled
	return_legend_patches:bool=False,
	lw:float=1.5,
	xrange=None,
	add_bins_title:bool=False,

	fig=None,
	ax=None,
	figsize:tuple=C_.PLOT_FIGSIZE,
	xlabel:str='values',
	ylabel:str='population',
	title:str='plot_hist_bins',
	xlim:tuple=[None, None],
	ylim:tuple=[None, None],
	grid:bool=True,
	grid_alpha:float=C_.PLOT_GRID_ALPHA,
	legend_loc:str='upper right',
	cmap=None,
	alpha:float=0.5,
	verbose:int=0,
	**kwargs):
	if histtype in ['step']:
		alpha=1

	fig, ax = plt.subplots(1,1, figsize=figsize, dpi=C_.PLOT_DPI) if fig is None else (fig, ax)
	if not isinstance(data_dict, dict):
		data_dict = {'distribution':data_dict} # transform in a dummy dict
	keys = list(data_dict.keys())
	cmap = cc.get_default_cmap(len(keys)) if cmap is None else cmap

	legend_patches = []
	for k,key in enumerate(keys):
		x = np.array(data_dict[key].copy())
		if verbose>0:
			x_samples, x_mean, x_std, x_min, x_max = len(x), np.mean(x), np.std(x), np.min(x), np.max(x)
			print(f'key: {key} - {C_.SAMPLES_TEXT}: {x_samples:,} - x_mean: {x_mean:.5f} - x_std: {x_std:.5f} - x_min: {x_min:.5f} - x_max: {x_max:.5f}')

		c = cmap.colors[k]
		hist_kwargs = {
			'density':uses_density,
			'color':c,
			'label':None,
			'alpha':alpha,
			'histtype':histtype,
			'range':xrange,
			'linewidth':linewidth,
		}
		n, new_bins, patches = ax.hist(x, bins, **hist_kwargs)

		p5 = np.percentile(x, 5)
		p50 = np.percentile(x, 50)
		p95 = np.percentile(x, 95)
		#label = f'{key}'+', $N='+f'{len(x):,}'+'$'+', $p_{50}='+f'{p50:.2f}'+'_{-'+f'{p50-p5:.2f}'+'}^{+'+f'{p95-p50:.2f}'+'}$'
		label = f'{key}'+' - $N: '+f'{len(x):,}'+'$'+' - $p_{50}: '+f'{p50:.2f}'+'_{-'+f'{p50-p5:.2f}'+'}^{+'+f'{p95-p50:.2f}'+'}$'
		#label = f'{key}'+' - $N: '+f'{len(x):,}'+'$'+' - $p50_{p5-p50}^{p95-p50}: '+f'{p50:.2f}'+'_{-'+f'{p50-p5:.2f}'+'}^{+'+f'{p95-p50:.2f}'+'}$'
		legend_patches.append(mpatches.Patch(color=c, label=label))

	ax.set_xlabel(xlabel)
	ax.set_ylabel('density' if uses_density and not ylabel is None else ylabel)
	bins_text = f' (bins: {bins:,})' if add_bins_title else ''
	ax.set_title(f'{title}{bins_text}')
	ax.set_xlim(xlim)
	ax.set_ylim(ylim)
	ax.legend(handles=legend_patches, loc=legend_loc, fontsize=12)

	if grid:
		xax = ax.set_axisbelow(True); ax.xaxis.grid(True, alpha=grid_alpha)
		yax = ax.set_axisbelow(True); ax.yaxis.grid(True, alpha=grid_alpha)

	if uses_annotation:
		magic_offset = 0.15
		max_ylim = ax.get_ylim()[1]
		new_max_ylim = max_ylim*(1+(len(keys))*magic_offset)
		ax.set_ylim([ax.get_ylim()[0], new_max_ylim])

		for k,key in enumerate(keys):
			x = np.array(data_dict[key].copy())
			c = cmap.colors[k]
			p50 = np.median(x)
			ann_y = new_max_ylim*0.9 - (k+1)*new_max_ylim*0.1
			#ann = ax.annotate(f'{ann_x:.4f}', xy=(ann_x, ann_y), xytext=(0, annotation_size), fontsize=5, ha='center',
			#		textcoords='offset points',
			#		size=annotation_size, va='center', color='w',
			#		bbox=dict(boxstyle='round', fc=c, ec='none', alpha=annotation_alpha),
			#		#arrowprops=dict(arrowstyle='wedge,tail_width=1.0', fc=c, ec='none', patchA=None, patchB=None, relpos=(0.5, 0.2), alpha=annotation_alpha),
			#		)
			ax.axvline(x=p5, ls='--', c=c, label=None, lw=lw)
			ax.axvline(x=p50, ls='-', c=c, label=None, lw=lw)
			ax.axvline(x=p95, ls='--', c=c, label=None, lw=lw)

	if return_legend_patches:
		return fig, ax, legend_patches
	return fig, ax
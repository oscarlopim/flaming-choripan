from __future__ import print_function
from __future__ import division
from . import C_

import numpy as np
import matplotlib.pyplot as plt
from ..lists import list_product
from ..files import create_dir

###################################################################################################################################################

def flat_axs(axs, x, y):
	return [axs[x_,y_] for x_,y_ in list_product(np.arange(0, x),np.arange(0, y))]

def save_fig(fig, save_filedir,
	close_fig_after_save=True,
	):
	if not save_filedir is None:
		save_rootdir = '/'.join(save_filedir.split('/')[:-1])
		create_dir(save_rootdir)
		plt.savefig(save_filedir)
		if close_fig_after_save:
			close_fig(fig)
	else:
		plt.show()

def close_fig(fig):
	plt.close(fig)
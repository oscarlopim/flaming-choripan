from __future__ import print_function
from __future__ import division
from __future__ import annotations
from . import C_

#from typing import List
import itertools
import random

###################################################################################################################################################

def list_product(*args):
	return list(itertools.product(*args)) # just a wrap

def flat_list(list_of_lists:List[list]):
	return sum(list_of_lists, [])

def get_random_item(l:list):
	return l[random.randint(0, len(l)-1)]

def get_random_key(d:dict):
	return get_random_item(list(d.keys()))

def get_augmented_random_list(l:list, n):
	return [get_random_item(l) for _ in range(n)]

class Iter():
	def __init__(self, *args):
		super().__init__()
		self.args = args

def search_Iter(d:dict, aux_d:dict):
	if isinstance(d, dict):
		keys = list(d.keys())
		for key in keys:
			search_Iter(d[key], aux_d)

	elif isinstance(d, Iter):
		indexs = list(range(len(d.args)))
		aux_d[id(d)] = indexs
	else:
		pass

def create_new_dict(d:dict, id_indexs:list):
	if isinstance(d, dict):
		keys = list(d.keys())
		new_dict = {}
		for key in keys:
			value = create_new_dict(d[key], id_indexs)
			new_dict[key] = value

		return new_dict

	elif isinstance(d, Iter):
		index = id_indexs[id(d)]
		return d.args[index]

	else:
		return d

def decompose_dict_Iter(d:dict):
	assert isinstance(d, dict)
	
	# search for Iter objects
	handler_dict = {}
	keys = list(d.keys())
	aux_dict = {}
	search_Iter(d, aux_dict)
	aux_dict_keys = list(aux_dict.keys())
	if len(aux_dict_keys)==0:
		return [d]

	iter_args = [aux_dict[k] for k in aux_dict_keys]
	prod = list(itertools.product(*iter_args))
	id_indexs_list = []
	for p in prod:
		id_indexs_list.append({k:p[n] for n,k in enumerate(aux_dict_keys)})
	#print(id_indexs)

	sub_dicts = []
	for id_indexs in id_indexs_list:
		#print(' >>>  id_indexs',id_indexs)
		new_dict = create_new_dict(d, id_indexs)
		#print(new_dict)
		sub_dicts.append(new_dict)

	return sub_dicts

def get_list_of_dicts(d:dict,
	add_id:bool=False,
	):
	'''
	Gets a product function by using the list from the input dictionary
	
	Parameters
	----------
	d (dict[lists]): It's a dictionary that contains, for every key, a list of elements.
					This list will be passed through a product function in order to get every possible values.
	'''
	list_dict = []
	args = []
	for k in d.keys():
		args.append(d[k])
		
	for j,arg in enumerate(list(itertools.product(*args))):
		sub_dict = {}
		for i,k in enumerate(d.keys()):
			if add_id:
				sub_dict['ID'] = j
			sub_dict[k] = arg[i]
		list_dict.append(sub_dict)

	return list_dict

def merge_lists(*args):
	merged = list(itertools.chain(*args))
	return merged
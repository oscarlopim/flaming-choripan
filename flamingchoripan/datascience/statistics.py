from __future__ import print_function
from __future__ import division
from . import C_

import numpy as np
import math

###################################################################################################################################################

class XError():
	def __init__(self, x, dim,
		error_scale=1,
		):
		self.x = np.array(x).copy()
		self.dim = dim
		self.error_scale = error_scale
		
		### calculate statistics
		self.mean = self.get_mean()
		self.median = self.set_percentile(50)
		self.std = self.get_std()
		self.serror = self.get_serror()
		
		for p in [1, 5, 10, 15]:
			self.set_percentile(p)
			self.set_percentile(100-p)

		self.dsymbols = {
			'std':C_.STD_LATEXCHAR,
			'serror':C_.SERROR_LATEXCHAR,
		}

	def set_percentile(self, p:int):
		percentile = np.percentile(self.x, p, axis=self.dim)
		setattr(self, f'p{p}', percentile)
		return percentile

	def get_mean(self):
		return np.mean(self.x, axis=self.dim)

	def get_std(self):
		std = np.std(self.x, axis=self.dim)*self.error_scale
		return std

	def get_serror(self):
		'''
		Standar Error = sqrt(sum((x-x_mean)**2)/(N-1)) / sqrt(N)
		'''
		assert len(self)>1
		serror = np.std(self.x, axis=self.dim, ddof=1)/math.sqrt(self.x.shape[self.dim])*self.error_scale
		return serror

	def get_symbol(self, attr):
		return f'{self.error_scale}{self.dsymbols[attr]}'

	def __len__(self):
		return self.x.shape[self.dim]

	def __repr__(self):
		txt = f'XError({self.mean:.2f}+-{self.std:.2f}, dim={self.dim})'
		return txt

###################################################################################################################################################

class TopRank():
    def __init__(self, name,
        print_n=10):
        self.name = name
        self.print_n = print_n
        self.names = []
        self.values = []
        
    def add(self, name, value):
        self.names.append(name)
        self.values.append(value)
        
    def calcule_rank(self):
        self.idxs = np.argsort(self.values)[::-1].tolist() # inverse to show high values firts
        
    def __len__(self):
        return len(self.names)

    def __repr__(self):
        txt = f'{self.name}:\n'
        idxs = self.idxs
        for k,idx in enumerate(idxs):
            txt += f'({k+1}) - {self.names[idx]}: {self.values[idx]}\n'
            if k+1>=self.print_n:
                break
        return txt[:-1]

###################################################################################################################################################

def get_linspace_ranks(x, samples_per_range):
	i = 0
	sx = np.sort(x)
	ex_ranges = []
	while i<len(sx):
		sub_sx = sx[i:i+samples_per_range]
		ex_ranges.append(sub_sx)
		#print(sx[i:i+samples_per_range])
		i += samples_per_range

	if len(sub_sx)<samples_per_range:
		ex_ranges = ex_ranges[:-1]

	assert len(ex_ranges)>=2
	ranks = [ex_ranges[k][-1]+(ex_ranges[k+1][0]-ex_ranges[k][-1])/2 for k in range(len(ex_ranges)-1)]
	ranks = [sx[0]] + ranks + [sx[-1]]
	#print('ranks',ranks)
	rank_ranges = np.array([(ranks[k], ranks[k+1]) for k in range(len(ranks)-1)])
	#print('rank_ranges',rank_ranges)
	index_per_range = [np.where((x>ranks_i) & (x<=ranks_f)) for ranks_i,ranks_f in rank_ranges]
	return rank_ranges, index_per_range, ranks

def dropout_extreme_percentiles(x,
	p=5,
	mode:str='both',
	):
	if mode=='both':
		valid_indexs = np.where((x>np.percentile(x, p)) & (x<np.percentile(x, 100-p)))
	elif mode=='lower': # dropout lower values
		valid_indexs = np.where(x>np.percentile(x, p))
	elif mode=='upper': # dropout upper values
		valid_indexs = np.where(x<np.percentile(x, 100-p))
	else:
		raise Exception(f'no mode {mode}')
	return x.copy()[valid_indexs], valid_indexs

def get_sigma_clipping_indexing(x, dist_mean, dist_sigma, sigma_m:float,
	apply_lower_bound:bool=True,
	):
	x = np.array(x)
	valid_indexs = np.ones(len(x)).astype(bool)
	valid_indexs &= x < dist_mean+dist_sigma*sigma_m # is valid if is in range
	if apply_lower_bound:
		valid_indexs &= x > dist_mean-dist_sigma*sigma_m # is valid if is in range
	return valid_indexs

def get_populations_cdict(labels, class_names):
	uniques, counts = np.unique(labels, return_counts=True)
	return {c:counts[list(uniques).index(c)] for c in class_names}
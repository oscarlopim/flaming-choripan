# flaming-choripan

<img src="img.png" width="400">

This is repository full of python functions, classes, methods, etc, that I've used along my adventure with Machine Learning, Data Science and programming in general.
Some of the uses are:
- strings, list, dicts, console prints methods
- progress bars
- pickle-files methods
- custom plots and colors
- basic data methods
- email utils
- jupyter-notebook
- latex tables methods

This is a very personal and experimental library, so use it with caution.
If, for any odd reason, you are using this repo, then please report any bugs.

Autor: Óscar Pimentel Fuentes
Email: oscarlo.pimentel@gmail.com